package net.pl3x.bukkit.pickyourpic.listener;

import net.minecraft.server.v1_12_R1.AxisAlignedBB;
import net.pl3x.bukkit.pickyourpic.PickYourPic;
import net.pl3x.bukkit.pickyourpic.configuration.Config;
import net.pl3x.bukkit.pickyourpic.task.RemoveRateLimit;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Art;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Painting;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;

import java.util.List;
import java.util.UUID;

public class PlayerListener implements Listener {
    private final PickYourPic plugin;

    public PlayerListener(PickYourPic plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onMouseWheel(PlayerItemHeldEvent event) {
        Player player = event.getPlayer();
        if (!player.isSneaking() || !player.hasPermission("pickyourpic.use")) {
            return;
        }

        // check if user is rate limited
        UUID uuid = player.getUniqueId();
        if (plugin.rateLimited.contains(uuid)) {
            return; // player is rate limited
        }

        // get painting
        Painting painting = getNearestPictureInSight(player);
        if (painting == null) {
            return;
        }

        Location paintingLoc = painting.getLocation();

        // check build rights
        BlockBreakEvent blockBreakEvent = new BlockBreakEvent(paintingLoc.getBlock(), player);
        Bukkit.getPluginManager().callEvent(blockBreakEvent);
        if (blockBreakEvent.isCancelled()) {
            return; // no build rights
        }

        // rate limit the user (prevents lag/high cpu)
        plugin.rateLimited.add(uuid);
        new RemoveRateLimit(uuid).runTaskLater(plugin, 5);

        int previous = event.getPreviousSlot();
        int next = event.getNewSlot();

        // detect mousewheel direction
        boolean reverse = previous - next > 0;
        if ((previous == 0 && next == 8) || (previous == 8 && next == 0)) {
            reverse = !reverse;
        }

        // cycle painting art
        Art art = painting.getArt();
        boolean fits = false;
        int counter = 0;
        int maxTries = Art.values().length + 10; // give an extra 10 tries just in case
        while (!fits) {
            if (counter > maxTries) {
                plugin.getLog().info("Could not make any painting fit. Giving up to prevent infinite loop crash.");
                return; // could not fit, give up trying
            }
            art = getNextArt(art, reverse);
            fits = painting.setArt(art, false);
            counter++;
        }

        // show painting name to player
        if (Config.ACTIONBAR_NAME) {
            player.sendActionBar(ChatColor.translateAlternateColorCodes('&',
                    "&aPainting&7: &6&o" + WordUtils.capitalize(art.name().toLowerCase()
                            .replace("_", " "))));
        }

        plugin.getLog().debug("Cycling painting at " + paintingLoc);
    }

    private Painting getNearestPictureInSight(Player player) {
        List<Entity> paintings = player.getNearbyEntities(5, 5, 5);

        // remove all entities that are not paintings
        // not a painting
        paintings.removeIf(entity -> !(entity instanceof Painting));

        // check line of sight
        List<Block> sight = player.getLineOfSight(null, 5);
        for (Block block : sight) {
            if (block.getType() != Material.AIR) {
                break; // do not look through walls
            }

            AxisAlignedBB blockBB = new AxisAlignedBB(
                    block.getLocation().getX(),
                    block.getLocation().getY(),
                    block.getLocation().getZ(),
                    block.getLocation().getX() + 1,
                    block.getLocation().getY() + 1,
                    block.getLocation().getZ() + 1);

            // the bounding or collision box of the block
            Location eye = player.getEyeLocation();
            for (Entity painting : paintings) {
                if (painting.getLocation().distance(eye) <= 5 &&
                        ((CraftEntity) painting).getHandle().getBoundingBox().intersects(blockBB)) {
                    return (Painting) painting;
                }
            }
        }
        return null;
    }

    private Art getNextArt(Art art, boolean reverse) {
        int current = art.ordinal();
        int max = Art.values().length - 1;
        int n = current + (reverse ? -1 : 1);

        if (n < 0) {
            n = max;
        } else if (n > max) {
            n = 0;
        }

        return Art.values()[n];
    }
}
