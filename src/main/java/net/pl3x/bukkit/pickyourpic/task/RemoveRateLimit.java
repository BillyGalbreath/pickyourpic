package net.pl3x.bukkit.pickyourpic.task;

import java.util.UUID;
import net.pl3x.bukkit.pickyourpic.PickYourPic;
import org.bukkit.scheduler.BukkitRunnable;

public class RemoveRateLimit extends BukkitRunnable {
    private final UUID uuid;

    public RemoveRateLimit(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public void run() {
        PickYourPic.getPlugin(PickYourPic.class).rateLimited.remove(uuid);
    }
}
