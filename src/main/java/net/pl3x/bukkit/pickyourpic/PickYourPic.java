package net.pl3x.bukkit.pickyourpic;

import net.pl3x.bukkit.pickyourpic.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class PickYourPic extends JavaPlugin {
    private final Logger logger;
    public final Set<UUID> rateLimited = new HashSet<>();

    public PickYourPic() {
        this.logger = new Logger(this);
    }

    public Logger getLog() {
        return logger;
    }

    @Override
    public void onEnable() {
        saveDefaultConfig();

        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

        getLog().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        getLog().info(getName() + " disabled.");
    }
}
